﻿using FacebookConnectorModule.Models;
using FacebookConnectorModule.Config;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookConnectorModule.Services
{
    public class ServiceAgentElastic
    {
        private Globals globals;

        public async Task<ResultItem<FacebookPostModelResult>> GetModel(FacebookPostQueryRequest request)
        {
            var taskResult = await Task.Run<ResultItem<FacebookPostModelResult>>(() =>
            {
                var result = new ResultItem<FacebookPostModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new FacebookPostModelResult()
                };

                if (string.IsNullOrEmpty(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "IdConfig is empty!";
                    return result;
                }

                if (!globals.is_GUID(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "IdConfig is no valid GUID!";
                    return result;
                }

                var getRootConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderRootConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRootConfig.GetDataObjects(getRootConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the root-config!";
                    return result;
                }

                if (!dbReaderRootConfig.Objects1.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Config cannot be found!";
                    return result;
                }

                result.Result.RootConfig = dbReaderRootConfig.Objects1.First();

                var searchSubConfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.RootConfig.GUID,
                        ID_RelationType = LocalData.ClassRel_Postquery__Facebook__contains_Postquery__Facebook_.ID_RelationType,
                        ID_Parent_Other = LocalData.ClassRel_Postquery__Facebook__contains_Postquery__Facebook_.ID_Class_Right
                    }
                };

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the sub-configs!";
                    return result;
                }

                if (dbReaderSubConfigs.ObjectRels.Any())
                {
                    result.Result.Configs.AddRange(dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }));
                }
                else
                {
                    result.Result.Configs.Add(result.Result.RootConfig);
                }

                var searchAccessTokens = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = LocalData.ClassRel_Postquery__Facebook__needs_Accesstoken.ID_RelationType,
                    ID_Parent_Other = LocalData.ClassRel_Postquery__Facebook__needs_Accesstoken.ID_Class_Right
                }).ToList();

                var dbReaderAccessTokens = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderAccessTokens.GetDataObjectRel(searchAccessTokens);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the access-tokens!";
                    return result;
                }

                result.Result.ConfigsToAccessTokens = dbReaderAccessTokens.ObjectRels;

                if (!result.Result.ConfigsToAccessTokens.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Accesstokens found!";
                    return result;
                }

                var searchConfigsToPatterns = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = LocalData.ClassRel_Postquery__Facebook__uses_Pattern.ID_RelationType,
                    ID_Parent_Other = LocalData.ClassRel_Postquery__Facebook__uses_Pattern.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToPattern = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToPattern.GetDataObjectRel(searchConfigsToPatterns);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Patterns!";
                    return result;
                }

                result.Result.ConfigsToPatterns = dbReaderConfigsToPattern.ObjectRels;

                if (!result.Result.ConfigsToPatterns.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Patterns!";
                    return result;
                }

                var searchPatternsOfPatterns = result.Result.ConfigsToPatterns.Select(ctP => new clsObjectAtt
                {
                    ID_Object = ctP.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Pattern_Pattern.ID_AttributeType
                }).ToList();

                if (!searchPatternsOfPatterns.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Patterns of Patterns!";
                    return result;
                }

                var dbReaderPatternsOfPatterns = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPatternsOfPatterns.GetDataObjectAtt(searchPatternsOfPatterns);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Parttners of Patterns!";
                    return result;
                }

                result.Result.PatternsPattern = dbReaderPatternsOfPatterns.ObjAtts;

                var searchConfigsToQueryLog = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = LocalData.ClassRel_Postquery__Facebook__Done_Query_Log__Facebook_Posts_.ID_RelationType,
                    ID_Parent_Other = LocalData.ClassRel_Postquery__Facebook__Done_Query_Log__Facebook_Posts_.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToQueryLog = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToQueryLog.GetDataObjectRel(searchConfigsToQueryLog);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Querylogs!";
                    return result;
                }

                result.Result.ConfigsToQueryLogs = dbReaderConfigsToQueryLog.ObjectRels;

                var searchQueryLogsToDateTimeStamp = result.Result.ConfigsToQueryLogs.Select(queryLog => new clsObjectAtt
                {
                    ID_Object = queryLog.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Query_Log__Facebook_Posts__DateTimestamp.ID_AttributeType
                }).ToList();

                if (searchQueryLogsToDateTimeStamp.Any())
                {
                    var dbReaderQueryLogsToDateTimeStamp = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderQueryLogsToDateTimeStamp.GetDataObjectAtt(searchQueryLogsToDateTimeStamp);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Datetimestamps of querylogs";
                        return result;
                    }

                    result.Result.QueryLogsDateTimeStamp = dbReaderQueryLogsToDateTimeStamp.ObjAtts;
                }

                var searchQueryLogsToSinces = result.Result.ConfigsToQueryLogs.Select(queryLog => new clsObjectRel
                {
                    ID_Object = queryLog.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Query_Log__Facebook_Posts__since_Unix_Timestamp.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Query_Log__Facebook_Posts__since_Unix_Timestamp.ID_Class_Right
                }).ToList();

                if (searchQueryLogsToSinces.Any())
                {
                    var dbReaderQueryLogsToSinces = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderQueryLogsToSinces.GetDataObjectRel(searchQueryLogsToSinces);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the since-stamps of querylogs";
                        return result;
                    }

                    result.Result.QueryLogsToSince = dbReaderQueryLogsToSinces.ObjectRels;
                }

                var searchQueryLogsToUntils = result.Result.ConfigsToQueryLogs.Select(queryLog => new clsObjectRel
                {
                    ID_Object = queryLog.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Query_Log__Facebook_Posts__until_Unix_Timestamp.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Query_Log__Facebook_Posts__until_Unix_Timestamp.ID_Class_Right
                }).ToList();

                if (searchQueryLogsToUntils.Any())
                {
                    var dbReaderQueryLogsToUntils = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderQueryLogsToUntils.GetDataObjectRel(searchQueryLogsToUntils);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Until-stamps of querylogs";
                        return result;
                    }

                    result.Result.QueryLogsToUntil = dbReaderQueryLogsToUntils.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals)
        {
            this.globals = globals;
        }
    }
}
