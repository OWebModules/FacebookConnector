﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookConnectorModule.Models
{
    public class FacebookPostQueryRequest
    {
        public string IdConfig { get; set; }

        public FacebookPostQueryRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
