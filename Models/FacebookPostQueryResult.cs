﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookConnectorModule.Models
{
    public class FacebookPostQueryResult
    {
        public int PostCounts { get; set; }
    }
}
