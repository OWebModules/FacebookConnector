﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookConnectorModule.Models
{
    public class FacebookPostModelResult
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ConfigsToAccessTokens { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToPatterns { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> PatternsPattern { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> ConfigsToQueryLogs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> QueryLogsDateTimeStamp { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> QueryLogsToSince { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> QueryLogsToUntil { get; set; } = new List<clsObjectRel>();


    }
}
