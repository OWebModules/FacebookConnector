﻿using ElasticSearchConfigModule;
using ElasticSearchNestConnector;
using Facebook;
using FacebookConnectorModule.Models;
using FacebookConnectorModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FacebookConnectorModule
{
    public class FacebookController : AppController
    {
        public async Task<ResultItem<FacebookPostQueryResult>> QueryPosts(FacebookPostQueryRequest request)
        {
            var taskResult = await Task.Run<ResultItem<FacebookPostQueryResult>>(async () =>
           {
               var result = new ResultItem<FacebookPostQueryResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new FacebookPostQueryResult()
               };

               var elasticAgent = new ServiceAgentElastic(Globals);
               var elasticSearchConfigController = new ElasticSearchConfigController(Globals);

               var resultModel = await elasticAgent.GetModel(request);
               result.ResultState = resultModel.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }



               foreach (var configItem in resultModel.Result.Configs)
               {

                   try
                   {
                       var elasticSearchConfig = await elasticSearchConfigController.GetConfig(configItem,
                       Config.LocalData.RelationType_belonging,
                       Config.LocalData.RelationType_belonging,
                       Globals.Direction_LeftRight);

                       if (elasticSearchConfig == null)
                       {
                           result.ResultState = Globals.LState_Error.Clone();
                           result.ResultState.Additional1 = "No elasticsearch index and/or type found!";
                           return result;
                       }

                       var accessToken = resultModel.Result.ConfigsToAccessTokens.FirstOrDefault(config => config.ID_Object == configItem.GUID);

                       if (accessToken == null)
                       {
                           result.ResultState = Globals.LState_Error.Clone();
                           result.ResultState.Additional1 = "No accesstoken found!";
                           return result;
                       }

                       var pattern = (from patternItem in resultModel.Result.ConfigsToPatterns
                                      join patternPattern in resultModel.Result.PatternsPattern on patternItem.ID_Other equals patternPattern.ID_Object
                                      select patternPattern).FirstOrDefault();

                       if (pattern == null)
                       {
                           result.ResultState = Globals.LState_Error.Clone();
                           result.ResultState.Additional1 = "No search-pattern provided!";
                           return result;
                       }

                       var lastLog = (from queryLog in resultModel.Result.ConfigsToQueryLogs.Where(cfg => cfg.ID_Object == configItem.GUID)
                                      join createStamp in resultModel.Result.QueryLogsDateTimeStamp on queryLog.ID_Other equals createStamp.ID_Object
                                      join since in resultModel.Result.QueryLogsToSince on queryLog.ID_Other equals since.ID_Object
                                      join until in resultModel.Result.QueryLogsToUntil on queryLog.ID_Other equals until.ID_Object
                                      select new
                                      {
                                          IdQueryLog = queryLog.ID_Other,
                                          NameQueryLog = queryLog.Name_Other,
                                          IdAttributeCreateStamp = createStamp.ID_Attribute,
                                          CreateStamp = createStamp.Val_Date,
                                          IdSince = since.ID_Other,
                                          Since = since.Name_Other,
                                          IdUntil = until.ID_Other,
                                          Until = until.Name_Other
                                      }).OrderByDescending(log => log.CreateStamp).FirstOrDefault();

                       var goOn = true;
                       var sinceVal = new DateTime(2011, 1, 1);
                       var untilVal = sinceVal.AddYears(1).AddDays(-1);
                       var query = pattern.Val_String.Replace($"@{Config.LocalData.Object_OItem_since.Name.ToUpper()}@", $"{sinceVal.Year}-{sinceVal.Month}-{sinceVal.Day}").
                                Replace($"@{Config.LocalData.Object_OItem_until1.Name.ToUpper()}@", $"{untilVal.Year}-{untilVal.Month}-{untilVal.Day}");

                       IDictionary<string, object> postData = null;
                       var dbSelector = new clsUserAppDBSelector(elasticSearchConfig.Result.NameServer, elasticSearchConfig.Result.Port, elasticSearchConfig.Result.NameIndex, Globals.SearchRange, Globals.Session);
                       var dbUpdater = new clsUserAppDBUpdater(dbSelector);
                       var facebookClient = new Facebook.FacebookClient(accessToken.Name_Other);
                       var documents = new List<clsAppDocuments>();
                       try
                       {
                           while (goOn)
                           {
                               if (documents.Count > 1000)
                               {
                                   dbUpdater.SaveDoc(documents, elasticSearchConfig.Result.NameType);
                                   documents.Clear();
                               }

                               postData = (IDictionary<string, object>)facebookClient.Get(query);

                               if (postData.Keys.Contains("posts"))
                               {
                                   JsonObject posts = (JsonObject)postData["posts"];

                                   if (posts.Keys.Contains("data"))
                                   {
                                       JsonArray data = (JsonArray)posts["data"];
                                       if (data.Count > 0)
                                       {
                                           foreach (JsonObject dataItem in data)
                                           {
                                               var doc = new clsAppDocuments
                                               {
                                                   Id = dataItem["id"].ToString(),
                                                   Dict = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(dataItem.ToString())
                                               };
                                               documents.Add(doc);
                                           }
                                       }
                                       else
                                       {
                                           goOn = false;
                                       }

                                   }

                                   //if (posts.Keys.Contains("paging"))
                                   //{
                                   //    JsonObject paging = (JsonObject)posts["paging"];
                                   //    var next = new Uri(paging["previous"].ToString());

                                   //    query = next.PathAndQuery;
                                   //    query = Regex.Replace(query, @"/v\d+\.\d+/", "");
                                   //}

                               }
                               else
                               {
                                   if (postData.Keys.Contains("data") || postData.Keys.Contains("paging"))
                                   {
                                       if (postData.Keys.Contains("data"))
                                       {

                                           JsonArray data = (JsonArray)postData["data"];
                                           if (data.Count > 0)
                                           {

                                               foreach (JsonObject dataItem in data)
                                               {
                                                   var doc = new clsAppDocuments
                                                   {
                                                       Id = dataItem["id"].ToString(),
                                                       Dict = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(dataItem.ToString())
                                                   };
                                                   documents.Add(doc);
                                               }
                                           }
                                           else
                                           {
                                               goOn = false;
                                           }

                                       }

                                       //if (postData.Keys.Contains("paging"))
                                       //{
                                       //    JsonObject paging = (JsonObject)postData["paging"];
                                       //    var next = new Uri(paging["next"].ToString());

                                       //    query = next.PathAndQuery;
                                       //    query = Regex.Replace(query, @"/v\d+\.\d+/", "");
                                       //}


                                   }
                                   else
                                   {
                                       goOn = false;
                                       //sinceVal = untilVal.AddDays(1);
                                       //untilVal = sinceVal.AddYears(1);
                                       //query = pattern.Val_String.Replace($"@{Config.LocalData.Object_OItem_since.Name.ToUpper()}@", $"{sinceVal.Year}-{sinceVal.Month}-{sinceVal.Day}").
                                       //     Replace($"@{Config.LocalData.Object_OItem_until1.Name.ToUpper()}@", $"{untilVal.Year}-{untilVal.Month}-{untilVal.Day}");
                                   }

                               }

                               if (untilVal == DateTime.Now.Date.AddDays(-1))
                               {
                                   break;
                               }

                               sinceVal = untilVal.AddDays(1);
                               if (sinceVal.Year == DateTime.Now.Year)
                               {
                                   untilVal = DateTime.Now.Date.AddDays(-1);
                               }
                               else
                               {
                                   untilVal = sinceVal.AddYears(1).AddDays(-1);
                               }
                               
                               query = pattern.Val_String.Replace($"@{Config.LocalData.Object_OItem_since.Name.ToUpper()}@", $"{sinceVal.Year}-{sinceVal.Month}-{sinceVal.Day}").
                                    Replace($"@{Config.LocalData.Object_OItem_until1.Name.ToUpper()}@", $"{untilVal.Year}-{untilVal.Month}-{untilVal.Day}");

                               
                           }

                           if (documents.Any())
                           {
                               dbUpdater.SaveDoc(documents, elasticSearchConfig.Result.NameType);
                               documents.Clear();
                           }


                       }
                       catch (Exception ex)
                       {
                           result.ResultState = Globals.LState_Error.Clone();
                           result.ResultState.Additional1 = "Error while getting the query-result!";
                           return result;

                       }
                   }
                   catch (Exception ex)
                   {

                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = ex.Message;
                       return result;
                   }



               }


               return result;
           });

            return taskResult;
        }

        public FacebookController(Globals globals) : base(globals)
        {
        }
    }
}
